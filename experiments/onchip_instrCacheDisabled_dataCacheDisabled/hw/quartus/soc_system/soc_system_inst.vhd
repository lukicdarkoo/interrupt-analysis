	component soc_system is
		port (
			clk_clk                            : in  std_logic                    := 'X';             -- clk
			pio_0_external_connection_in_port  : in  std_logic_vector(9 downto 0) := (others => 'X'); -- in_port
			pio_0_external_connection_out_port : out std_logic_vector(9 downto 0);                    -- out_port
			reset_reset_n                      : in  std_logic                    := 'X'              -- reset_n
		);
	end component soc_system;

	u0 : component soc_system
		port map (
			clk_clk                            => CONNECTED_TO_clk_clk,                            --                       clk.clk
			pio_0_external_connection_in_port  => CONNECTED_TO_pio_0_external_connection_in_port,  -- pio_0_external_connection.in_port
			pio_0_external_connection_out_port => CONNECTED_TO_pio_0_external_connection_out_port, --                          .out_port
			reset_reset_n                      => CONNECTED_TO_reset_reset_n                       --                     reset.reset_n
		);

