
module soc_system (
	clk_clk,
	pio_0_external_connection_in_port,
	pio_0_external_connection_out_port,
	reset_reset_n);	

	input		clk_clk;
	input	[9:0]	pio_0_external_connection_in_port;
	output	[9:0]	pio_0_external_connection_out_port;
	input		reset_reset_n;
endmodule
