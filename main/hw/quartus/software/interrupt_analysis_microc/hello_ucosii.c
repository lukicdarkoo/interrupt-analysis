/*************************************************************************
 * Copyright (c) 2004 Altera Corporation, San Jose, California, USA.      *
 * All rights reserved. All use of this software and documentation is     *
 * subject to the License Agreement located at the end of this file below.*
 **************************************************************************
 * Description:                                                           *
 * The following is a simple hello world program running MicroC/OS-II.The *
 * purpose of the design is to be a very simple application that just     *
 * demonstrates MicroC/OS-II running on NIOS II.The design doesn't account*
 * for issues such as checking system call return codes. etc.             *
 *                                                                        *
 * Requirements:                                                          *
 *   -Supported Example Hardware Platforms                                *
 *     Standard                                                           *
 *     Full Featured                                                      *
 *     Low Cost                                                           *
 *   -Supported Development Boards                                        *
 *     Nios II Development Board, Stratix II Edition                      *
 *     Nios Development Board, Stratix Professional Edition               *
 *     Nios Development Board, Stratix Edition                            *
 *     Nios Development Board, Cyclone Edition                            *
 *   -System Library Settings                                             *
 *     RTOS Type - MicroC/OS-II                                           *
 *     Periodic System Timer                                              *
 *   -Know Issues                                                         *
 *     If this design is run on the ISS, terminal output will take several*
 *     minutes per iteration.                                             *
 **************************************************************************/

#include <stdio.h>
#include "includes.h"
#include "altera_avalon_timer_regs.h"
#include "altera_avalon_pio_regs.h"
#include "system.h"
#include "sys/alt_irq.h"
#include "sys/alt_stdio.h"

#define USE_EXTERNAL_ANALYSER 1

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK task1_stk[TASK_STACKSIZE];
OS_STK task2_stk[TASK_STACKSIZE];
/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
#define TASK2_PRIORITY      2

#define QUEUE_SIZE 10

OS_EVENT *ISRSem;
OS_FLAG_GRP *ISRFlag;
OS_FLAGS flags;
OS_EVENT *ISRmail;
OS_EVENT *ISRqueue;

INT8U err;
unsigned int stop, start;
unsigned int ignore;

typedef struct {
 unsigned char button_number;
} msg;
msg* msg_queue[10];
msg some_msg;


void task_semaphore(void* pdata) {
	while (1) {
		OSSemPend(ISRSem, 0, &err);

		// Write a tick to parallel port
#if USE_EXTERNAL_ANALYSER
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x01);
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x00);
#else
		IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, 9);
		stop = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);
		alt_printf("Semaphore interrupt delay: 0x%x\n", start - stop);
#endif
	}
}

void task_flag(void* pdata) {
	while (1) {
		OSFlagPend(ISRFlag, 0x07, OS_FLAG_WAIT_SET_ALL + OS_FLAG_CONSUME, 0, &err);

#if USE_EXTERNAL_ANALYSER
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x01);
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x00);
#else
		IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, 9);
		stop = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);
		alt_printf("F: %x\n ", start - stop);
#endif
		//OSFlagPost(ISRFlag, 1, OS_FLAG_SET, &err);
		//alt_printf("Killing the task\n");
		//break;
		for (int i = 0; i < 1000000; i++);

		alt_printf("test\n");
	}
}

void task_mail(void* pdata) {
	msg* bla;

	while (1) {
		bla = (struct msg*) OSMboxPend(ISRmail, 0, &err);
#if USE_EXTERNAL_ANALYSER
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x01);
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x00);
#else
		IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, 9);
		stop = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);
#endif

		alt_printf("Getting message in %x cycles with button number %x\n", start - stop, bla->button_number);
	}
}


void task_queue(void* pdata) {
	INT8U err;
	msg* bla;

	while (1) {
		bla = (struct msg*) OSQPend(ISRqueue, 0, &err);

#if USE_EXTERNAL_ANALYSER
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x01);
		IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x00);
#else
		IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, 9);
		stop = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);

		alt_printf("Getting message from queue in : %x cycles with button number %x\n",start - stop, bla->button_number);
#endif
	}
}

static void parallel_isr_logic_analyzer_response(void* context) {
	unsigned int res = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
	res = ((~res) >> 1) & 0x07;
	alt_printf("From interrupt routine. Key combination: 0x%x\n", res);
	some_msg.button_number = res;

#if USE_EXTERNAL_ANALYSER
	IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x02);
	IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 2, 0x00);
#else
	IOWR_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE, 9);
	start = IORD_ALTERA_AVALON_TIMER_SNAPL(TIMER_0_BASE);
#endif

	// OSSemPendAbort(ISRSem, OS_PEND_OPT_BROADCAST, &err);
	// OSFlagPost(ISRFlag, res, OS_FLAG_SET, &err);
	// OSMboxPost(ISRmail, &some_msg);
	OSQPost(ISRqueue, &some_msg);

	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0x0f);
}


int main(void) {
	OSInit();

	// Create semaphore
	ISRSem = OSSemCreate(1);
	ISRFlag = OSFlagCreate(0, &err);
	ISRmail = OSMboxCreate(NULL);
	ISRqueue = OSQCreate(msg_queue, QUEUE_SIZE);

	// Configure custom parallel port
	IOWR_8DIRECT(PARALLEL_PORT_0_BASE, 0, 0xFF);

	// Configure Altera timer
	IOWR_ALTERA_AVALON_TIMER_PERIODL(TIMER_0_BASE, 0xffff);
	IOWR_ALTERA_AVALON_TIMER_PERIODH(TIMER_0_BASE, 0x0000);
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,2);
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,6);

	// Configure Altera parallel port
	IOWR_ALTERA_AVALON_PIO_DIRECTION(PIO_0_BASE, 0x00);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PIO_0_BASE, 0x0f);
	alt_ic_isr_register(PIO_0_IRQ_INTERRUPT_CONTROLLER_ID, PIO_0_IRQ,
			parallel_isr_logic_analyzer_response, NULL, NULL);




	// Start Task1
	OSTaskCreateExt(task_queue,
	NULL, (void *) &task1_stk[TASK_STACKSIZE - 1],
	TASK1_PRIORITY,
	TASK1_PRIORITY, task1_stk,
	TASK_STACKSIZE,
	NULL, 0);


	/*
	 OSTaskCreateExt(task2,
	 NULL,
	 (void *)&task2_stk[TASK_STACKSIZE-1],
	 TASK2_PRIORITY,
	 TASK2_PRIORITY,
	 task2_stk,
	 TASK_STACKSIZE,
	 NULL,
	 0);
	 */

	OSStart();
	return 0;
}

/******************************************************************************
 *                                                                             *
 * License Agreement                                                           *
 *                                                                             *
 * Copyright (c) 2004 Altera Corporation, San Jose, California, USA.           *
 * All rights reserved.                                                        *
 *                                                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a     *
 * copy of this software and associated documentation files (the "Software"),  *
 * to deal in the Software without restriction, including without limitation   *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
 * and/or sell copies of the Software, and to permit persons to whom the       *
 * Software is furnished to do so, subject to the following conditions:        *
 *                                                                             *
 * The above copyright notice and this permission notice shall be included in  *
 * all copies or substantial portions of the Software.                         *
 *                                                                             *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
 * DEALINGS IN THE SOFTWARE.                                                   *
 *                                                                             *
 * This agreement shall be governed in all respects by the laws of the State   *
 * of California and by the laws of the United States of America.              *
 * Altera does not recommend, suggest or require that this reference design    *
 * file be used in conjunction or combination with any other product.          *
 ******************************************************************************/
